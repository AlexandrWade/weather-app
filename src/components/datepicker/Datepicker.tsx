import React, {FC} from "react";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

interface DatepickerProps {
    date: Date;
    onChange: (date: Date) => void;
}

const Datepicker: FC<DatepickerProps> = ({date, onChange}) => {
    return (
        <DatePicker
            selected={date}
            onChange={onChange}
            inline
        />
    )
}

export default Datepicker;