import React, {FC} from "react";

export interface ITab {
    id: number;
    name: string;
}

interface TabsProps {
    tabsList: ITab[];
    selected: number;
    onChange: (id: number) => void;
}

const Tabs: FC<TabsProps> = ({tabsList, selected, onChange}) => {
    return (
        <ul className="flex flex-wrap text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:border-gray-700 dark:text-gray-400">
            {tabsList.map(({ id, name }) => (
                <li className="mr-2" key={id}>
                    {id === selected ? (
                        <a role="tab" className="inline-block p-4 text-blue-600 bg-gray-100 rounded-t-lg active dark:bg-gray-800 dark:text-blue-500">{name}</a>
                    ) : (
                        <a role="tab" className="cursor-pointer inline-block p-4 rounded-t-lg hover:text-gray-600 hover:bg-gray-50 dark:hover:bg-gray-800 dark:hover:text-gray-300" onClick={() => onChange(id)}>{name}</a>
                    )}
                </li>
            ))}
        </ul>
    )
}

export default Tabs;