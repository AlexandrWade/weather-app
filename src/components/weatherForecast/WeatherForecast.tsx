import React, {FC} from "react";
import {addLeadingZero} from "../../utils";

import {useGetForecastQuery} from "../../services/weatherService";
import WeatherForecastDaily from "./WeatherForecastDaily";
import {ICity} from "../../types/types";

const WEATHER_FORECAST_PERIOD = 8;

interface WeatherForecastProps {
    city: ICity;
    date: Date;
}

const WeatherForecast: FC<WeatherForecastProps> = ({city, date}) => {
    let startDate = date;
    let endData = new Date(date);
    endData.setDate(endData.getDate() + (WEATHER_FORECAST_PERIOD - 1));  //-1 because current day also included in result, so we need one day less in result

    const {error, isLoading, isFetching, data} = useGetForecastQuery({
        lat: city.lat,
        long: city.long,
        dateStart: `${startDate.getFullYear()}-${addLeadingZero(startDate.getMonth() + 1)}-${addLeadingZero(startDate.getDate())}`,
        dateEnd: `${endData.getFullYear()}-${addLeadingZero(endData.getMonth() + 1)}-${addLeadingZero(endData.getDate())}`,
    })

    const getDayDate = (dayNumber) => {
        let startDate = date;
        let endData = new Date(date);
        endData.setDate(startDate.getDate() + dayNumber);
        return endData;
    }

    let forecastByDays = [];
    if (data) {
        forecastByDays = [...Array(WEATHER_FORECAST_PERIOD)].map((empty, dayNumber) =>
            <WeatherForecastDaily
                key={dayNumber}
                weatherCode={data.daily.weathercode[dayNumber]}
                temperatureMin={data.daily.temperature_2m_min[dayNumber]}
                temperatureMax={data.daily.temperature_2m_max[dayNumber]}
                temperatureUnits={data.daily_units.temperature_2m_max}
                windSpeedMax={data.daily.windspeed_10m_max[dayNumber]}
                windSpeedUnits={data.daily_units.windspeed_10m_max}
                windDirection={data.daily.winddirection_10m_dominant[dayNumber]}
                windDirectionUnits={data.daily_units.winddirection_10m_dominant}
                sunrise={data.daily.sunrise[dayNumber]}
                sunset={data.daily.sunset[dayNumber]}
                date={getDayDate(dayNumber)}
            />
        )
    }

    return (
        <>
            <div className="relative py-4">
                {error ? (
                    <div className="alert bg-red-100 rounded-md py-5 px-6 mb-3 text-base text-red-700 inline-flex items-center w-full" role="alert">
                        {'status' in error ? (
                            <>
                                <strong className="mr-1">Error - {error.status}</strong> {error.data.reason}
                            </>
                        ) : (
                            <strong>{error.message}</strong>
                        )}
                    </div>

                ) : null}

                {!isLoading && isFetching && (
                    <div className="p-4 absolute top-0 left-0 w-full min-h-full flex justify-center cursor-progress z-30">
                        <div className="absolute top-0 left-0 w-full min-h-full bg-white opacity-60"/>

                        <div className="self-center py-8 z-10">
                            <div className="text-4xl text-blue-500">
                                Fetching...
                            </div>
                        </div>
                    </div>
                )}

                {data && (
                    <>
                        <div className="grid gap-4 grid-cols-4">
                            {forecastByDays}
                        </div>
                    </>
                )}
            </div>

            {data && (
                <p className="text-sm text-slate-400 mt-8">* - timezone {data.timezone}</p>
            )}
        </>
    )
}

export default WeatherForecast;