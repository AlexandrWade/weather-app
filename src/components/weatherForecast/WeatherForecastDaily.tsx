import React, {FC} from "react";
import WEATHER_CODES from "../../constants/weatherCodes";
import {IWeatherCode} from "../../types/types";
import {number} from "prop-types";

interface WeatherForecastDailyProps {
    weatherCode: number;
    temperatureMin: string;
    temperatureMax: string;
    temperatureUnits: string;
    windSpeedMax: string;
    windSpeedUnits: string;
    windDirection: string;
    windDirectionUnits: string;
    sunrise: string;
    sunset: string;
    date: Date;
}

const WeatherForecastDaily: FC<WeatherForecastDailyProps> =
    ({
         weatherCode, temperatureMin, temperatureMax, temperatureUnits, windSpeedMax, windSpeedUnits, windDirection, windDirectionUnits, sunrise, sunset, date
    }) => {
    const getWeatherDescription = (code: number) => {
        let codeDescription: IWeatherCode = WEATHER_CODES.find(weatherCode => weatherCode.code === code);

        return codeDescription ? codeDescription.name : '';
    }

    const formatDateToTime = (date: string) => {
        return new Intl.DateTimeFormat('ru-RU', {hour: '2-digit', minute: '2-digit', hour12: false}).format(new Date(date));
    }

    return (
        <div className="border border-blue-100 shadow-lg shadow-blue-500/30 hover:shadow-indigo-500/40 rounded-md p-4 basis-1/4 flex-initial transition ease-in-out hover:-translate-y-1 hover:scale-110 duration-300 z-10 hover:z-20 relative bg-white">
            <p className="text-lg mb-2">{new Intl.DateTimeFormat('en-US', { weekday: 'long'}).format(date)} - {new Intl.DateTimeFormat('ru-RU').format(date)}</p>

            <p className="text-xl mb-2">{getWeatherDescription(weatherCode)}</p>

            <p>
                Temperature: min <span className="font-bold">{temperatureMin}{temperatureUnits}</span> max <span className="font-bold">{temperatureMax}{temperatureUnits}</span>
            </p>

            <p>
                Wind Speed: <span className="font-bold">{windSpeedMax} {windSpeedUnits}</span>
            </p>

            <p>
                Wind Direction: <span className="font-bold">{windDirection}{windDirectionUnits}</span>
            </p>

            <p>
                Sunrise: <span className="font-bold">{formatDateToTime(sunrise)}</span>
            </p>

            <p>
                Sunset: <span className="font-bold">{formatDateToTime(sunset)}</span>
            </p>
        </div>
    )
}

export default WeatherForecastDaily;