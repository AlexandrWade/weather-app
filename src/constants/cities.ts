import {ICity} from "../types/types";

const CITIES: ICity[] = [
    {
        id: 1,
        name: 'Kryvyi Rih',
        lat: '47.96',
        long: '33.44'
    },
    {
        id: 2,
        name: 'New York',
        lat: '40.71',
        long: '-74.01'
    },
    {
        id: 3,
        name: 'London',
        lat: '51.51',
        long: '-0.13'
    },
    {
        id: 4,
        name: 'Berlin',
        lat: '52.52',
        long: '13.41'
    },
    {
        id: 5,
        name: 'Paris',
        lat: '48.85',
        long: '2.35'
    },
    {
        id: 6,
        name: 'Tokyo',
        lat: '35.69',
        long: '139.69'
    }
];

export default CITIES;