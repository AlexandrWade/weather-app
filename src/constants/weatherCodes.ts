//from api https://open-meteo.com/en/docs#api-documentation

import {IWeatherCode} from "../types/types";

const WEATHER_CODES: IWeatherCode[] = [
    {
        id: 1,
        code: 0,
        name: 'Clear sky',
    },
    {
        id: 2,
        code: 1,
        name: 'Mainly clear',
    },
    {
        id: 3,
        code: 2,
        name: 'Partly cloudy',
    },
    {
        id: 4,
        code: 3,
        name: 'Overcast',
    },
    {
        id: 5,
        code: 45,
        name: 'Fog',
    },
    {
        id: 6,
        code: 48,
        name: 'Depositing rime fog',
    },
    {
        id: 7,
        code: 51,
        name: 'Light drizzle',
    },
    {
        id: 8,
        code: 53,
        name: 'Moderate drizzle',
    },
    {
        id: 9,
        code: 55,
        name: 'Dense intensity drizzle',
    },
    {
        id: 10,
        code: 56,
        name: 'Light freezing drizzle',
    },
    {
        id: 11,
        code: 57,
        name: 'Dense intensity freezing drizzle',
    },
    {
        id: 12,
        code: 61,
        name: 'Slight rain',
    },
    {
        id: 13,
        code: 63,
        name: 'Moderate rain',
    },
    {
        id: 14,
        code: 65,
        name: 'Heavy intensity rain',
    },
    {
        id: 15,
        code: 66,
        name: 'Light freezing rain',
    },
    {
        id: 16,
        code: 67,
        name: 'Heavy intensity freezing rain',
    },
    {
        id: 17,
        code: 71,
        name: 'Slight snow fall',
    },
    {
        id: 18,
        code: 73,
        name: 'Moderate snow fall',
    },
    {
        id: 19,
        code: 75,
        name: 'Heavy intensity snow fall',
    },
    {
        id: 20,
        code: 77,
        name: 'Snow grains',
    },
    {
        id: 21,
        code: 80,
        name: 'Slight rain showers',
    },
    {
        id: 22,
        code: 81,
        name: 'Moderate rain showers',
    },
    {
        id: 23,
        code: 82,
        name: 'Violent rain showers',
    },
    {
        id: 24,
        code: 85,
        name: 'Slight snow showers',
    },
    {
        id: 25,
        code: 86,
        name: 'Heavy snow showers',
    },
    {
        id: 26,
        code: 95,
        name: 'Thunderstorm',
    },
    {
        id: 27,
        code: 96,
        name: 'Thunderstorm with slight hail',
    },
    {
        id: 28,
        code: 99,
        name: 'Thunderstorm with heavy hail',
    },
];

export default WEATHER_CODES;