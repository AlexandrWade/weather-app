import type {NextPage} from 'next';
import Head from 'next/head';
import { useState, useMemo } from 'react';

import Datepicker from '../components/datepicker/Datepicker';
import WeatherForecast from '../components/weatherForecast/WeatherForecast';

import CITIES from '../constants/cities';
import Tabs, {ITab} from "../components/tabs/Tabs";

const IndexPage: NextPage = () => {
    const [selectedDate, setSelectedDate] = useState<Date>(new Date());
    const [selectedTabID, setSelectedTabID] = useState<number>(CITIES[0].id);

    const citiesTabs: ITab[] = useMemo(() => CITIES.map(city => ({
        id: city.id,
        name: city.name,
    })), []);

    return (
        <div className="container mx-auto">
            <Head>
                <title>Weather App</title>
            </Head>

            <header className="py-16">
                <h1 className="text-8xl text-blue-gray-100">Weather App</h1>
            </header>

            <div className="flex flex-row mt-8">
                <div className="flex-auto">
                    <Tabs
                        tabsList={citiesTabs}
                        selected={selectedTabID}
                        onChange={(value) => setSelectedTabID(value)}
                    />

                    <WeatherForecast
                        city={CITIES.find(city => city.id === selectedTabID)}
                        date={selectedDate}
                    />
                </div>

                <div className="flex-none ml-8">
                    <Datepicker
                        date={selectedDate}
                        onChange={(date) => setSelectedDate(date)}
                    />
                </div>
            </div>
        </div>
    )
}

export default IndexPage
