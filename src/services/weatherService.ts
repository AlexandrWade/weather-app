import {
    BaseQueryFn,
    createApi, FetchArgs,
    fetchBaseQuery
} from '@reduxjs/toolkit/query/react';

interface ApiError {
    data: {
        error: boolean;
        reason: string;
    };
    status: number;
}

// Define a service using a base URL and expected endpoints
export const weatherApi = createApi({
    reducerPath: 'weatherApi',
    baseQuery: fetchBaseQuery(
        {
            baseUrl: 'https://api.open-meteo.com/v1'
        }
    ) as BaseQueryFn<string | FetchArgs, unknown, ApiError, {}>,
    endpoints: (builder) => ({
        getForecast: builder.query({
            // don't work because of comma in daily parameter
            // query: ({lat, long, date}) => ({
            //     url: '/forecast',
            //     params: {
            //         latitude: lat,
            //         longitude: long,
            //         start_date: date,
            //         end_date: date,
            //         daily: 'weathercode,temperature_2m_max,temperature_2m_min',
            //         timezone: 'auto',
            //     }
            // }),
            query: ({lat, long, dateStart, dateEnd}) => `/forecast?latitude=${lat}&longitude=${long}&start_date=${dateStart}&end_date=${dateEnd}&timezone=Europe%2FMoscow&daily=weathercode,temperature_2m_max,temperature_2m_min,sunrise,sunset,windspeed_10m_max,winddirection_10m_dominant`,
        }),
    }),
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const {useGetForecastQuery} = weatherApi;