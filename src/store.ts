import {configureStore} from '@reduxjs/toolkit';

import {weatherApi} from "./services/weatherService";

export function makeStore() {
    return configureStore({
        reducer: {
            [weatherApi.reducerPath]: weatherApi.reducer,
        },
        middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(weatherApi.middleware),
    })
}

const store = makeStore();

export default store;
