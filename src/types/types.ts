export interface ICity {
    id: number;
    name: string;
    lat: string;
    long: string;
}

export interface IWeatherCode {
    id: number;
    code: number;
    name: string;
}