export const addLeadingZero = (date) => {
    return date.toString().padStart(2, '0');
}